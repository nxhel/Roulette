import java.util.Random;
public class RouletteWheel {
    private Random rand;
    private int lastSpin=0;

    public RouletteWheel()
    {
        this.rand= new Random();
    }
    public void spin()
    {
      this.lastSpin=rand.nextInt(37);
    }
    public int getValue()
   {
        return this.lastSpin;
    }
    
    public Boolean checkIfWin(int numberGuess, int randomNumber)
    {
      boolean isWin=false;
      if(numberGuess==randomNumber)
      {
        isWin=true;
      }
      else
      {
        isWin=false;
      }
      return isWin;
    }
 }
