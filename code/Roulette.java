import java.util.Scanner;
public class Roulette{
  public static final Scanner scan= new Scanner(System.in);
    public static void main(String[] args) 
    {
       System.out.println("WELCOME TO NIHEL'S ROULE ROULETTE!!! \n");
       RouletteWheel wheelObj = new RouletteWheel();

       int playerBalance= 1000;
       int payOutFactor =35;
       int playerBet=0;
       int totalGamesPlayed=0;
       int winRounds=0;
       boolean gameOver= false;

       while(!gameOver)
       {
         System.out.println("Would you like to PLACE a BET ?");
         String placeBetChoice= scan.next();  
         
         if (placeBetChoice.toUpperCase().charAt(0)=='Y')
         {
            System.out.println("how much would you like to BET ");
            playerBet= scan.nextInt();
            
            while(playerBet>playerBalance)
            {
                System.out.println("try again, your balance IS " + playerBalance);
                playerBet=scan.nextInt();
            }    
        }
        else
        {
            System.out.println("SAFE CHOICE, NO BET FOR YOU");
            playerBet=0;
        }
        
        System.out.println("what number ON THE ROULETTE would you like to BET on?");
        int landingGuess= scan.nextInt();
        
        while (landingGuess>37 || landingGuess<0)
        {
          System.out.println("Impossible to bet on THE CHOSEN NUMBER , POSSIBLE NUMBERS START FROM 0 to 36, try again");
          landingGuess= scan.nextInt();
        }
        
        wheelObj.spin(); 
        int randWheelNumber=wheelObj.getValue();
        totalGamesPlayed++;
        
        if(wheelObj.checkIfWin(landingGuess,randWheelNumber))
        {
          System.out.println("... magnificent Win");
          playerBalance += playerBet * payOutFactor;
          System.out.println("Your balance is NOW " + playerBalance + " $CAD");
          winRounds++;
        }
        else
        {
          System.out.println("... ohH nNoo the lucky number WAS " + randWheelNumber);
          playerBalance -= playerBet;
          System.out.println("Your balance is NOW " + playerBalance + " $CAD ");
        }
        System.out.println("PRESS 2 TO QUIT OR ANY OTHER NUMBER TO CONTINUE");
        int continueGame= scan.nextInt();
        
        if(continueGame==2)
        {
          gameOver=true;
        }
       }
        System.out.println("You have reached the END...");
        System.out.println("GOOD GAME :)) You WON " + winRounds + " ON " + totalGamesPlayed + " games PLAYED ");
        System.out.println("You are leaving the game WITH " +playerBalance + " $CAD");
    }   
}